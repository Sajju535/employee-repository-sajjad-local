package com.loverspart.demo.application.services;

import com.loverspart.demo.application.domain.Employee;
import com.loverspart.demo.application.dto.EmployeeDTO;
import com.loverspart.demo.application.interfaces.services.EmployeeService;
import com.loverspart.demo.application.repositories.EmployeeRepository;

public class EmployeeServiceImpl implements EmployeeService {

	EmployeeRepository employeeRepository;

	Employee employee;

	@Override
	public EmployeeDTO insertEmployeeDetails(EmployeeDTO employeeDTO) {
		 employee = EmployeeDTO.valueOf(employeeDTO);
		 employee= employeeRepository.save(employee);
	     return Employee.valueOf(employee);
	
	}

}
