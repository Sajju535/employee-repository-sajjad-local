package com.loverspart.demo.application.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.loverspart.demo.application.domain.Employee;
import com.loverspart.demo.application.dto.EmployeeDTO;
import com.loverspart.demo.application.interfaces.services.EmployeeService;

@Controller
@RequestMapping(value = "/employee-management")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@PutMapping(value = "/employee")
	public EmployeeDTO addEmployee(@RequestBody EmployeeDTO employeeDTO) {
		return employeeService.insertEmployeeDetails(employeeDTO);
	}

}
