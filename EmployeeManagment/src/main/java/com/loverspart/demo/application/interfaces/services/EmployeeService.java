package com.loverspart.demo.application.interfaces.services;

import com.loverspart.demo.application.domain.Employee;
import com.loverspart.demo.application.dto.EmployeeDTO;

public interface EmployeeService {

	EmployeeDTO insertEmployeeDetails(EmployeeDTO employeeDTO);

}
