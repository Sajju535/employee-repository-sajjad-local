package com.loverspart.demo.application.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.loverspart.demo.application.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee,Long>{

}
